from ..watchdog.edges import edges as watchdog_edges
from ..masterswitch.edges import edges as masterswitch_edges
from ..damping.edges import edges as damping_edges
from ..isolation.edges import edges as isolation_edges

edges = []
edges += watchdog_edges
edges += masterswitch_edges
edges += damping_edges
edges += isolation_edges

edges += [
        # state A       -->     state B 
        ('READY', 'ENGAGE_DAMPING_LOOPS'),
        ('DISENGAGE_DAMPING_LOOPS', 'READY'),
        ('DAMPED', 'LOAD_CART_BIAS_FOR_ISOLATION'),
        ('LOAD_CART_BIAS_FOR_ISOLATION', 'DAMPED'),
        ('DEISOLATING', 'DAMPED'),
        ('INIT','READY'),
        ]
